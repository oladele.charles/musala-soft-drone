import { object, string, ref, number } from "yup";

// - serial number (100 characters max);
// - model (Lightweight, Middleweight, Cruiserweight, Heavyweight);
// - weight limit (500gr max);
// - battery capacity (percentage);
// - state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING).

// Each **Medication** has: 
// - name (allowed only letters, numbers, ‘-‘, ‘_’);
// - weight;
// - code (allowed only upper case letters, underscore and numbers);
// - image (picture of the medication case).


export const LoadMedicationSchema = object({
  name: string().required("name is required").matches(/^[aA-zZ-_\s]+$/, "name allows only letters, numbers, -, _ "),
  weight: string().required("weight is required"),
  code: string().required("code is required").matches(/^[A-Z_\s]+$/, "code allows only upper case letters, underscore and numbers"),
  DroneSerialNumber: string().required("DroneSerialNumber is required"),
});

export const RegisterDroneSchema = object({
  serialNumber: string().required("serialNumber is required").max(100,"serialNumber can't be more than 100 characters"),
  state: string()
    .required("state is required")
    .test("customTest", "state can be either IDLE, LOADING,LOADED, DELIVERING,DELIVERED or RETURNING", (value: any) => {
      if (value === null || value === undefined) {
        return false;
      }
      if (
        value === "IDLE" ||
        value === "LOADING" ||
        value === "LOADED" ||
        value === "DELIVERING" ||
        value === "DELIVERED" ||
        value === "RETURNING"
      ) {
        return value;
      }
      return false;
   
    }),
  weight: number().required("weight is required").max(500,"weight can't be more than 500gr"),
  battery: number().required("battery is required").max(100,"battery can't be more than 100"),
  model: string().required("battery is required")
  .test("customTest", "model can be either Lightweight, Middleweight,Cruiserweight or Heavyweight", (value: any) => {
    if (value === null || value === undefined) {
      return false;
    }
    if (
      value === "Lightweight" ||
      value === "Middleweight" ||
      value === "Cruiserweight" ||
      value === "Heavyweight"
    ) {
      return value;
    }
    return false;
  }),
});


export const MedicationByDroneSchema = object({
  droneSerialNumber: string().required("droneSerialNumber is required"),
});

export const DroneBatterySchema = object({
  droneSerialNumber: string().required("droneSerialNumber is required"),
});

