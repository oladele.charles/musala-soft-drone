import { DroneI } from "../controller/drone/interfaces";
import { MedicationI } from "../controller/medication/interfaces";


const sqlite3 = require("sqlite3").verbose();
let DB = new sqlite3.Database(":memory:");

export const AddDrone = async (details: DroneI) => {
  const drone = {
    ...details,
    is_active: false,
  };

  const insert = await DB.run(
    `INSERT INTO drones(serialNumber,
  model,
  weight,
  battery,
  state,      
  
  is_active) VALUES(?,?,?,?,?,?)`,
    [
      details.serialNumber,
      details.model,
      details.weight,
      details.battery,
      details.state,
      false,
    ]
  ,
    function (err: any) {
      if (err) {
        console.log(err.message);
        return false;
      }
      return true;
      // get the last insert id
      // console.log(`A row has been inserted with rowid ${this.lastID}`);
    }
  );
  console.log(insert);
  DB.close();

  return drone;
};

export const AddMedication = async (details: MedicationI) => {
  const insert = await DB.run(
    `INSERT INTO medications(  
    name,
    weight,
    code,
    imagePath,
    droneSerialNumber,
  
  is_active) VALUES(?,?,?,?,?,?)`,
    [
      details.name,
      details.weight,
      details.code,
      details.imagePath,
      details.droneSerialNumber,
      false,
    ]
  ,
    function (err: any) {
      if (err) {
        console.log(err.message);
        return false;
      }
      return true;
      // get the last insert id
      // console.log(`A row has been inserted with rowid ${this.lastID}`);
    }
  );
  console.log(insert);
  DB.close();

  const medication = {
    ...details,

    is_active: true,
  };

  return medication;
};
