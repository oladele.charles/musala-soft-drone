const sqlite3 = require("sqlite3").verbose();
let DB = new sqlite3.Database(":memory:");

export const Init = async () => {
  console.log(".....creating drone....");
  const state = [
    "IDLE",
    "LOADING",
    "LOADED",
    "DELIVERING",
    "DELIVERED",
    "RETURNING",
  ];
  const model = ["Lightweight", "Middleweight", "Cruiserweight", "Heavyweight"];

  await DB.run(
    `CREATE TABLE IF NOT EXISTS drones (    
      serialNumber SERIAL PRIMARY KEY,
    model VARCHAR NOT NULL
    ,weight INTEGER NOT NULL
    ,battery INTEGER NOT NULL
    ,state VARCHAR NOT NULL
    ,date_created DATE
    ,is_active Boolean default false)`,
    [],
    function (err: any) {
      if (err) {
        console.log("drones db error");
        return console.error(err.message);
      }
      DB.run(
        `INSERT INTO drones(serialNumber,
      model,
      weight,
      battery,
      state,      
      
      is_active) VALUES
      (?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?),
      (?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?)`,
        [
          "123456",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123457",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123458",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123459",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123460",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123461",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,
          "123462",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123463",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123464",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,

          "123465",
          model[Math.floor(Math.random() * 4)],
          Math.floor(Math.random() * 500) + 1,
          Math.floor(Math.random() * 100) + 1,
          state[Math.floor(Math.random() * 6)],
          false,
        ],
        function (err: any, result: any) {
          if (err) {
            console.log("db error");
            return console.error(err.message);
          }
          console.log(`Drones inserted `);
        }
      );
    }
  );
  await DB.run(
    `CREATE TABLE IF NOT EXISTS medications (    
      med_id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
    ,weight INTEGER NOT NULL    
    ,code VARCHAR NOT NULL
    ,imagePath VARCHAR NOT NULL
    ,droneSerialNumber VARCHAR NOT NULL
    ,date_created DATE
    ,is_active Boolean)`,
    [],
    function (err: any) {
      if (err) {
        console.log("medications db error");
        return console.error(err.message);
      }
    }
  );

  DB.close();
  
};

// export const OpenDB = new sqlite3.Database(":memory:", (err: any) => {
//   if (err) {
//     return console.error(err.message);
//   }
//   console.log("Connected to the in-memory SQlite database.");
// });

// // Init();

// export const CloseDB = DB.close((err: any) => {
//   if (err) {
//     return console.error(err.message);
//   }
//   console.log("Close the database connection.");
// });
