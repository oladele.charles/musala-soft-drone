
import { DroneI } from "../controller/drone/interfaces";
import { MedicationI } from "../controller/medication/interfaces";
const sqlite3 = require("sqlite3").verbose();
let DB = new sqlite3.Database(":memory:");


require("dotenv").config();

export const GetMedication = async (droneSerialNumber: string) => {
  const sql = `select *
from medications
where  
droneSerialNumber =?`;
  const medications = await DB.all(sql, [droneSerialNumber]);
  DB.close();
  //   ,
  //   (err: any, rows: MedicationI[]) => {
  //     if (err) {
  //       throw err;
  //     }
  //     return rows;
  //     // rows.forEach((row) => {
  //     //   console.log(row.name);
  //     // });
  //   }
  // );

  return medications;
};

export const GetDroneBattery = async (droneSerialNumber: string) => {
  const sql = `select battery
  from drones
  where  droneSerialNumber=?`;
  const drone_battery = await DB.get(sql, [droneSerialNumber]);
  DB.close();
  return drone_battery;
};

export const GetDroneWeight = async (droneSerialNumber: string) => {
  const sql = `select weight
  from drones
  where  droneSerialNumber=?`;
  const drone_weight = await DB.get(sql, [droneSerialNumber]);
  DB.close();
  return drone_weight;
};

export const GetAvailableDrones = async () => {
  const sql = `select weight
  from drones
  where  is_active =false`;
  const drone_weight = await DB.all(sql, []);
  DB.close();
  return drone_weight;
};
