import { Express } from "express";

import {
  AvailabeDroneReq,
  DroneBatteryReq,
  RegisterDroneReq,
  GetMedicationReq,
  LoadMedicationReq,
} from "./controller";

import {
  DroneBatterySchema,
  RegisterDroneSchema,
  LoadMedicationSchema,
  MedicationByDroneSchema,
} from "./middleware/schema";

import validateRequest from "./middleware/validateRequest";

const initRoute = "/api/mosula/drone";
const Route = function (app: Express) {
  app.post(
    initRoute + "/get_drone_battery",
    validateRequest(DroneBatterySchema),
    DroneBatteryReq
  );

  app.post(
    initRoute + "/register_drone",
    validateRequest(RegisterDroneSchema),
    RegisterDroneReq
  );

  app.post(
    initRoute + "/load_medication",
    validateRequest(LoadMedicationSchema),
    LoadMedicationReq
  );
  app.post(
    initRoute + "/get_medication",
    validateRequest(MedicationByDroneSchema),
    GetMedicationReq
  );
  app.get(initRoute + "/get_available_drone", AvailabeDroneReq);

  app.get(initRoute + "/", function (req, res, next) {
    res.status(200).json({ message: "Single Paycode Shit Happens 💩" });
  });
};

export default Route;
