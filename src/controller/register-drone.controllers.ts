import { NextFunction, Response } from "express";
import { Logger } from "../middleware/logger";
import droneClass from "./drone/drone";
import { DroneI } from "./drone/interfaces";

export const RegisterDroneReq = async (
  req: any,
  res: Response
) => {
  const body: DroneI = req.body;

  try {
    
    Logger.info({ type: "RegisterDroneReq ", ...req.body });

    const result = await droneClass.registerDrone(body);

    return res.send({ status: true, response: "success", result });
  } catch (error: any) {
    console.error(error);
    Logger.error({
      type: "RegisterDroneReq---Error",
      mess: JSON.stringify(error),
    });

    return res.send({
      status: false,
      message: error.message,
      response: error.response.data,
      result: null,
    });
  }
};
