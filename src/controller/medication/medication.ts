
import {  AddMedication, GetMedication } from "../../db-sqlite";
import {
  MedicationMethodI,
  
  MedicationI
} from "./interfaces";

class MedicationClass implements MedicationMethodI {



  async loadMedication(request: MedicationI) {
    const result=await AddMedication(request);
    return result;
  }
  async getMedicationByDrone(droneSerialNumber: string) {
    const result=await GetMedication(droneSerialNumber);
    return result;
  }

}


export default new MedicationClass;