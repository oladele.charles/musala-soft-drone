export interface MedicationMethodI {
  loadMedication: (data: MedicationI) => Promise<MedicationI|string>;
  getMedicationByDrone: (
    droneSerialNumber: string
  ) => Promise<MedicationI[] | []>;
}

export interface MedicationI {
  name: string;
  weight: number;
  code: string;
  imagePath: string;
  droneSerialNumber: string;
  // date_created: Date;
  // is_active: boolean;
}

export interface ResponseI {
  code: string;

  message: string;
}
