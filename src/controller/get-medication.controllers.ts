import { NextFunction, Response } from "express";
import { Logger } from "../middleware/logger";
import medicationClass from "./medication/medication";
import { DroneI } from "./drone/interfaces";

export const GetMedicationReq = async (
  req: any,
  res: Response
) => {
  const {droneSerialNumber} = req.body;

  try {
    
    Logger.info({ type: "GetMedicationReq ", ...req.body });

    const result = await medicationClass.getMedicationByDrone(droneSerialNumber);

    return res.send({ status: true, response: "success", result });
  } catch (error: any) {
    console.error(error);
    Logger.error({
      type: "GetMedicationReq---Error",
      mess: JSON.stringify(error),
    });

    return res.send({
      status: false,
      message: error.message,
      response: error.response.data,
      result: null,
    });
  }
};
