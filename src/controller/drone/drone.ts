import { AddDrone, GetAvailableDrones, GetDroneBattery } from "../../db-sqlite";
import { DroneMethodI, DroneI } from "./interfaces";

class DroneClass implements DroneMethodI {
  async registerDrone(request: DroneI) {
    const result = await AddDrone(request);
    return result;
  }

  async getAvailableDrones() {
    const result = await GetAvailableDrones();
    return result;
  }
  async getDroneBattery(droneSerialNumber: string) {
    const result = await GetDroneBattery(droneSerialNumber);
    const res=result.length>0?result[0]:null;
    return res;
  }
}

export default new DroneClass();
