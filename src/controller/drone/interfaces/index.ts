export interface DroneMethodI {
  registerDrone: (data: DroneI) => Promise<DroneI>;

  getAvailableDrones: () => Promise<DroneI[] | []>;
  getDroneBattery: (
    droneSerialNumber: string
  ) => Promise<{ battery: string } | null>;
}

// - checking loaded medication items for a given drone;
// - checking available drones for loading;
// - check drone battery level for a given drone;

// - name (allowed only letters, numbers, ‘-‘, ‘_’);
// - weight;
// - code (allowed only upper case letters, underscore and numbers);
// - image (picture of the medication case).

export interface DroneI {
  serialNumber: string;
  model: "Lightweight" | "Middleweight" | "Cruiserweight" | "Heavyweight";
  weight: number;
  battery: number;
  state:
    | "IDLE"
    | "LOADING"
    | "LOADED"
    | "DELIVERING"
    | "DELIVERED"
    | "RETURNING";
    // date_created:Date;
    // is_active:boolean;
}


export interface ResponseI {
  code: string;

  message: string;
}
