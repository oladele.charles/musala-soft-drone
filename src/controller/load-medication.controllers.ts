import { NextFunction, Response } from "express";
import { Logger } from "../middleware/logger";
import medicationClass from "./medication/medication";
import { MedicationI } from "./medication/interfaces";
import { GetDroneBattery, GetDroneWeight } from "../db-sqlite";

export const LoadMedicationReq = async (req: any, res: Response) => {
  const body: MedicationI = req.body;

  try {
    Logger.info({ type: "LoadMedicationReq ", ...req.body });
    let result = null;
    const drone_weight = await GetDroneWeight(body.droneSerialNumber);

    if (drone_weight > body.weight) {
      result = "medication weight greater than recommended drone weight";
    }
    const drone_battery = await GetDroneBattery(body.droneSerialNumber);
    if (drone_battery < 25) {
      result = "drone battery below 25%";
    }
    if (result == null) {
      result = await medicationClass.loadMedication(body);
    }
    return res.send({ status: true, response: "success", result });
  } catch (error: any) {
    console.error(error);
    Logger.error({
      type: "LoadMedicationReq---Error",
      mess: JSON.stringify(error),
    });

    return res.send({
      status: false,
      message: error.message,
      response: error.response.data,
      result: null,
    });
  }
};
