import {  Response } from "express";
import { Logger } from "../middleware/logger";
import droneClass from "./drone/drone";
import { DroneI } from "./drone/interfaces";

export const DroneBatteryReq = async (
  req: any,
  res: Response
) => {
  const {droneSerialNumber} = req.body;

  try {
    
    Logger.info({ type: "DroneBatteryReq ", ...req.body });

    const result = await droneClass.getDroneBattery(droneSerialNumber);

    return res.send({ status: true, response: "success", result });
  } catch (error: any) {
    console.error(error);
    Logger.error({
      type: "DroneBatteryReq---Error",
      mess: JSON.stringify(error),
    });

    return res.send({
      status: false,
      message: error.message,
      response: error.response.data,
      result: null,
    });
  }
};
