import { Response } from "express";
import { Logger } from "../middleware/logger";
import droneClass from "./drone/drone";
import { DroneI } from "./drone/interfaces";

export const AvailabeDroneReq = async (req: any, res: Response) => {
  try {
    const result = await droneClass.getAvailableDrones();

    return res.send({ status: true, response: "success", result });
  } catch (error: any) {
    console.error(error);
    Logger.error({
      type: "AvailabeDroneReq---Error",
      mess: JSON.stringify(error),
    });

    return res.send({
      status: false,
      message: error.message,
      response: error.response.data,
      result: null,
    });
  }
};
