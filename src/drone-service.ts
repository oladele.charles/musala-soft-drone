require("dotenv").config();
import { Logger } from "./middleware/logger";
import { Init } from "./db-sqlite/connect";
import express from "express";
import cors from "cors";
import Route from "./routes";

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

Route(app);

const port = process.env.PORT || 3200;

const drone_api = app.listen(port, () => {
  Logger.info(`Server listing at port ${port}`);
  Init();
});

export default drone_api;
